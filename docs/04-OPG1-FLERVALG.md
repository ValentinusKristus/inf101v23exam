[Generell informasjon](./03-INFO.md) &bullet; [README](../README.md) &bullet; [Oppgave 2: Tetromino-fabrikken](./05-OPG2-TETROMINOFABRIKKEN.md)
# Oppgave 1: flervalg

> Viktig: denne oppgaven skal besvares i Inspera (vurdering.uib.no)

For hver av påstandene under, avgjør om påstanden er sann eller usann.

Det gis 1.1 poeng for hvert riktig svar, og -0.9 poeng for både gale svar og ubesvarte rader. Hvis du er usikker på noe har du altså ingenting å tape på å gjette.

Du kan få maksimalt 10 poeng, og kan ikke få negative poeng på oppgaven. Dette systemet innebærer at du får maksimal score hvis du har alt riktig, og 0 poeng hvis du har 4 eller færre riktige svar.

| Avgjør om påstanden er sann eller usann  |
|-------------------------------------------|
| Standard-implementasjonen av equals-metoden gir alltid false som svar  |
| Hvis en klasse Milk implementerer grensesnittet Drinkable, da vil en variabel med typen Drinkable kunne peke på et objekt i klassen Milk uten at vi ser advarsler eller får feil.   |
| Et grensesnitt definerer en type og angir hvilke metoder som er tilgjengelig hvis man benytter typen. |
| Et objekt kan ha flere klasser. |
| En instansvariabel kan ha forskjellig verdi for ulike objekter i samme klasse.  |
| En klasse kan ha mer enn én konstruktør. |
| En instansvariabel kan ikke eksistere uten å tilhøre et objekt |
| Poenget med å definere en klasse er alltid å opprette objekter i klassen. Dersom man ikke oppretter objekter i en klasse, kunne man like gjerne fjernet den fra kildekoden uten at man merket forskjell. |
| To objekter i samme klasse kan implementere ulike grensesnitt. |
| Alle metoder i Java (inkludert dem som er static) har automatisk tilgang til variabelen `this`. |

